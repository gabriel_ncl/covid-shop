<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tutoriel', function () {
    return view('tuto');
});

Route::resource('articles', 'ArticleController');

Route::get('/contact', function () {
    return view('contact');
});

Route::post('/contact', function () {
    return view('contact');
});

Route::get('/paypal', function () {
    return view('paypal');
});

Route::get('/paid', function () {
    return view('paid');
});

Route::get('/messages', function () {
    return view('messages');
})->middleware('admin');


/* Cart routes */
Route::get('/cart', function () {
    return view('panier');
})->middleware('auth');

Route::get('/cart', 'CartController@index')->name('cart.index');

Route::post('/cart/ajouter','CartController@store')->name('cart.store');

Route::get('/videpanier', function() {
    Cart::destroy();
    return back();
});

Route::delete('/cart/{rowId}', 'CartController@destroy')->name('cart.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
