<?php

try {
    $bdd = new PDO('mysql:host=127.0.0.1:3308;dbname=site_database;charset=utf8mb4', 'root', '');
    $bdd->query('SET lc_time_names = "fr_FR"');
	}

catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
    echo $e;
}

$messages = $bdd->query('SELECT * FROM contacts')

?>


@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection

<div class="container_page">
    <h1 class="text-center" id="main_title">Messages :</h1>

    <div class="container_content">
        <div class="container">
            
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">id</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Message</th>
                  </tr>
                </thead>
                <tbody>
                    <?php while($m = $messages->fetch(PDO::FETCH_ASSOC)) {?>
                  <tr>
                    <th scope="row"><?= $m['id'] ?></th>
                    <td><?= $m['prenom'] ?></td>
                    <td><?= $m['email'] ?></td>
                    <td><?= substr($m['message_contact'], 0, 20).'...' ?></td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              
        </div>
    </div>
</div>

@section('scripts')
@endsection
