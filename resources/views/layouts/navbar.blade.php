<nav class="navbar navbar-expand-lg navbar-light bg-navbar fixed-top">
    <a href="/" class="navbar-brand" href="#">Covid Shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item">
                <a href="/" class="nav-link">Accueil <span class="sr-only">(current)</span></a>
            </li>

            <li class="nav-item">
                <a href="/articles" class="nav-link">Boutique</a>
            </li>

            <li class="nav-item">
                <a href="/tutoriel" class="nav-link">Tutoriel</a>
            </li>

            <li class="nav-item">
                <a href="/contact" class="nav-link">Contact</a>
            </li>


        </ul>
        @if (Route::has('login'))
            <div class="top-right links">
                @auth
                    <a href={{route ('articles.create')}} class="nav-item">Créer article</a>
                    <a href="messages" class="nav-item">Voir les messages</a>
                    <a href="{{route ('cart.index')}}" class="fas fa-shopping-cart"> <span
                            class="badge badge-pill badge-dark">{{ Cart::count() }}</span></a>
                    <a href="{{ url('/logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Se Déconnecter
                    </a>

                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @else
                    <a href="{{ route('login') }}">Se Connecter</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">S'enregistrer</a>
                    @endif
                @endauth
            </div>
        @endif
    </div>
</nav>
