<?php

try {
    $bdd = new PDO('mysql:host=127.0.0.1:3308;dbname=site_database;charset=utf8mb4', 'root', '');
    $bdd->query('SET lc_time_names = "fr_FR"');
	}

catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
    echo $e;
}

if(isset($_POST['contact_form'])) {

    $prenom = htmlentities($_POST['firstname']);
    $email = htmlentities($_POST['email']);
    $message_contact = htmlentities($_POST['message']);

    if(!empty($prenom) && !empty($email) && !empty($message_contact)) {

        $nouveau_contact = $bdd->prepare("INSERT INTO contacts (prenom, email, message_contact) VALUES (?, ?, ?)");
        $nouveau_contact->execute([$prenom, $email, $message_contact]);

        $message_success = "Votre message a bien été envoyé";

    } else {
        $message_success = "Une erreur s'est produite lors de l'envoie du message";
    }
}

?>

@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection

<div class="container_page">
    <h1 class="text-center" id="main_title">Contact</h1>

    <div class="container_content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Contactez-nous') }}</div>

                        <div class="card-body">

                            <form method="POST" action="">

                                <?php if(isset($message_success)) {
                                    if($message_success === "Votre message a bien été envoyé") {
                                        echo "<div class='alert alert-success'>$message_success</div>";
                                    } else {
                                        echo "<div class='alert alert-danger'>$message_success</div>";
                                    }
                                } ?>

                                <br>

                                <div class="form-group row">
                                    <label for="firstname" class="col-md-4 col-form-label text-md-right">Prénom</label>

                                    <div class="col-md-6">
                                        <input id="firstname" type="text" class="form-control" name="firstname" required>

                                        @error('firstname')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">Adresse mail</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" required>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="message" class="col-md-4 col-form-label text-md-right">Message</label>

                                    <div class="col-md-6">
                                        <textarea id="message" type="text" class="form-control" name="message" required></textarea>

                                        @error('message')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <input class="btn btn-primary" type="submit" name="contact_form" value="Envoyer">
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
@endsection
