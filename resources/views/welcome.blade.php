@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection

<div class="parallax">
    <div class="row h-100">
        <div class="col-md-12 align-self-center">
            <h2 class="text-center text-white">Bienvenue sur notre site !</h2>
            <h4 class="text-center text-white shadow p-3 mb-5 bg-transparent rounded"> Ceci est un site qui vise à la
                vente de produits pouvant protéger la popultaion contre le virus COVID-19 </h4>
        </div>
    </div>
</div>

<div class="contenu_page">
    <h2 class="text-center text-black"><b>Qui sommes nous ?</b></h2>
    <hr>

    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner carousel_size">
            <div class="carousel-item active img-fluid">
                <img class="d-block w-100" src="eva.png" alt="First slide">
            </div>
            <div class="carousel-item img-fluid">
                <img class="d-block w-100" src="gab.png" alt="Second slide">
            </div>
            <div class="carousel-item img-fluid">
                <img class="d-block w-100" src="lucas.jpg" alt="Third slide">
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>

    <div class="contenu_etudiant">
        <div id="text_etudiant" class="etudiant_unique">
            <h3 class="text-black">Nous sommes un groupe d'étudiants du CESI Ecole d'ingénieur de Sophia Antipolis ayant
                pour
                ambition de créer une organisation humanitaire durant cette période sanitaire difficile.
                C'est pour quoi nous nous lançons dans la production de gels hydro-alcooliques ainsi que la mise en
                place d'une plateforme
                web pour venir en aide aux citoyens dans le besoin d'équipements de protection. </h3>
        </div>
        <div id="logo_etudiant" class="etudiant_unique">
            <div class="etudiant_wrapper">
                <img id="img_etudiant" src="cesi.webp" alt="logo">
            </div>
        </div>
    </div>

    <br>

    <h2 class="text-center text-black"><b>Ce que nous faisons ?</b></h2>
    <hr>
    <h3 class="text-center text-black mt-5 p-5">Le but de notre organisation est de permettre aux citoyens les plus
        démunis de
        se procurer du gel hydro-alcoolique à des prix les plus faibles possibles pour rendre ces produits accessibles à
        tous.</h3>

    <br>

    <h2 class="text-center text-black"><b>Comment avoir fixé des prix aussi bas ?</b></h2>
    <hr>
    <h3 class="text-center text-black mt-5 p-5">De nombreuses stratégies ont été pensées afin de rendre le prix de nos
        produits aussi
        compétitifs. <br>En effet, une partie des gels disponibles sur le catalogue sont faits maison par nos soins pour
        limiter le coût
        que nécessiterait l'organisation si l'on revendait seulement les produits aux mêmes prix que les grandes
        enseignes de distribution.
        Pour ne pas se retrouver en rupture de stock, nous collaborons à présent avec un fabricant de gels
        hydro-alcooliques situé dans le centre de Nice. Les produits
        provenant de ce fabricant resteront à un prix tout aussi faible que les autres car nous nous sommes engagés à
        limiter au maximum la marge sur chaque produits.
        Et pour finir, le coût de livraison restera lui aussi très minimes car nos livraisons ne dépasseront pas pour
        l'instant la région PACA.</h3>

    <br>

    <h2 class="text-center text-black"><b>Notre partenaire ?</b></h2>
    <hr>
    <div class="contenu_partenaire">
        <div class="partenaire_unique">
            <h3 id="texte_partenaire" class="text-black">Face à cette crise la société Capvital collabore avec nous pour
                faciliter l'accès
                au plus grand nombre de personnes, des gels hydro-alcooliques.</h3>
        </div>
        <div id="partenaire_wrapper" class="partenaire_unique">
            <img id="logo_partenaire" src="capvital-santé.jpg" alt="logo">
        </div>
    </div>

</div>
@extends('layouts.footer')

@section('scripts')
@endsection
