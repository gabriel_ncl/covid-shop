@extends('layouts.master')
    @extends('layouts.navbar')

    @section('title')

        Covid Shop

    @endsection

    @section('styles')
    @endsection



    @section('content')
    <h1 class="text-center" id="main_title">Panier</h1>
        @if (Cart::count()> 0)
            <div class="px-4 px-lg-0">

                <div class="pb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 p-5 bg-white rounded shadow-sm mb-5">

                                <!-- Shopping cart table -->
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col" class="border-0 bg-light">
                                                <div class="p-2 px-3 text-uppercase">Article</div>
                                            </th>
                                            <th scope="col" class="border-0 bg-light">
                                                <div class="py-2 text-uppercase">Prix</div>
                                            </th>
                                            <th scope="col" class="border-0 bg-light">
                                                <div class="py-2 text-uppercase">Quantité</div>
                                            </th>
                                            <th scope="col" class="border-0 bg-light">
                                                <div class="py-2 text-uppercase">Effacer</div>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach (Cart::content() as $article)
                                        <?php global $total;
                                        $total = $total + $article->model->price * $article->qty?>
                                        <tr>
                                            <th scope="row">
                                                <div class="p-2">
                                                    <img src="{{ $article->model->imageUrl }}" alt="" width="70" class="img-fluid rounded shadow-sm">

                                                    <div class="ml-3 d-inline-block align-middle">
                                                        <h5 class="mb-0"><a href="#" class="text-dark d-inline-block">{{$article->model->name}}</a>
                                                    </div>
                                                </div>
                                            <td class="align-middle"><strong>{{$article->model->price }} €</strong></td>
                                            <td class="align-middle"><strong>{{$article->qty }}</strong></td>
                                            <td class="align-middle">
                                                <form action= "{{ route('cart.destroy', $article->rowId) }}" method ="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type='submit' class="text-dark"><i class="fa fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- End -->
                            </div>
                        </div>
                        <?php
                        $taxes = 20 * $total / 100;
                        $port = 2;

                        ?>
                        <div class="col-rg-6">
                            <div class="bg-light rounded-pill px-4 py-3 text-uppercase font-weight-bold"> Récapitulatif de la
                                commande
                            </div>
                            <div class="p-4">
                                <p class="font-italic mb-4"> Les frais d'expédition et les suppléments sont calculés en fonction du
                                    prix total saisi.</p>
                                <ul class="list-unstyled mb-4">
                                    <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted">
                                            Sous-total de la commande </strong><strong>{{ $total }} €</strong></li>
                                            <?php $total = $total + $taxes + $port; ?>
                                    <li class="d-flex justify-content-between py-3 border-bottom"><strong class="text-muted"> Frais
                                            de port</strong><strong>{{ $port }} €</strong></li>
                                    <li class="d-flex justify-content-between py-3 border-bottom"><strong
                                    class="text-muted">Taxes</strong><strong>{{ $taxes }} €</strong></li>
                                    <li class="d-flex justify-content-between py-3 border-bottom"><strong
                                            class="text-muted">Total</strong>
                                        <h5>{{ $total }} €</h5>
                                    </li>
                                </ul>

                                <div id="paypal-button"></div>
                                <script src="https://www.paypalobjects.com/api/checkout.js"></script>
                                <script>
                                var prix = <?php echo json_encode($total); ?>;
                                paypal.Button.render({
                                    // Configure environment
                                    env: 'sandbox',
                                    client: {
                                    sandbox: 'AYhEzvUoP3x6Gy5Ux73iYEEUeT6rlqMKvp5Rn7-NGSFwW4nMGh1ta9zoj0659b_5AQ8b0SvFTUMrTxCs',
                                    production: 'demo_production_client_id'
                                    },
                                    // Customize button (optional)
                                    locale: 'en_US',
                                    style: {
                                    size: 'small',
                                    color: 'blue',
                                    shape: 'pill',
                                    },

                                    // Enable Pay Now checkout flow (optional)
                                    commit: true,

                                    // Set up a payment
                                    payment: function(data, actions) {
                                    return actions.payment.create({
                                        transactions: [{
                                        amount: {
                                            total: prix,
                                            currency: 'EUR'
                                        }
                                        }]
                                    });
                                    },
                                    // Execute the payment
                                    onAuthorize: function(data, actions) {
                                    return actions.payment.execute().then(function() {
                                        // Show a confirmation message to the buyer
                                        /*window.location.href = "http://localhost:8000/paid?total_price=<?php echo json_encode($total); ?>";*/
                                        window.alert('Merci pour votre commande !');

                                    });
                                    }
                                }, '#paypal-button');

                                </script>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @else
            <div class="col-md-12">
                <p>Votre panier est vide.</p>
            </div>

        @endif
    @endsection

    @extends('layouts.footer')

    @section('scripts')
@endsection
