@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection

<h1 class="text-center" id="main_title">Notre tutoriel</h1>

<div class="container_article">
    <div class="container_image">

    </div>
    <div class="container_tuto">
        <h2>Comment faire un gel anti-bactérien soi-même ?</h2>

        <hr>
        <br>

        <p class="size">L'hygiène des mains est primordiale pour se protéger contre le coronavirus. Pour se laver les
            mains en extérieur : le gel hydroalcoolique. En pénurie dans de nombreuses pharmacies, on le fabrique soi
            même.
            Découvrez notre recette facile et rapide pour faire soi même un hydroalcoolique. </p>

        <br>

        <p class="small_titles"><b>Matériel nécessaire :</b></p>

        <ul class="size">
            <li>Flacon (30ml)</li>
            <li>Gel d'aloe vera ( hydratation )</li>
            <li>Huile essentielle de tea tree ( anti-bactérien, antiviral )</li>
            <li>Huile essentielle de lavande ( parfum + anti-bactérien )</li>
            <li>Huile végétale ( huile de jojoba, huile de lin, huile de cameline... )</li>
        </ul>

        <br>

        <p class="small_titles"><b>Les étapes à suivre :</b></p>

        <ol class="size">
            <li>Verser dans un bol 30ml de gel d'aloe vera</li>
            <li>Ajouter 5ml d'huile végétale</li>
            <li>Ajouter 10 gouttes d'huile essentielle de tea tree</li>
            <li>Ajouter 10 gouttes d'huile essentielle de lavande</li>
            <li>Ajouter 15ml d'alcool à 70 ou 90°</li>
            <li>Mélanger</li>
        </ol>
    </div>

    <div class="fournisseur">
        <h2 id="fournisseur">Les produits venant de notre fournisseur :</h2>

        <div class="contenu_produits">
            <a href="https://www.onatera.com/produit-gel-aloe-vera-bio-99-3-1-l-orfito,16006.html?LGWCODE=16006;105739;2445&utm_source=google&utm_medium=cpc&utm_campaign=search&gclid=Cj0KCQjwudb3BRC9ARIsAEa-vUubyt038LS8-vkpRC-zolPRoBuhK3-e6ROxicuK0AFf-g2gou8_7g4aAkbCEALw_wcB&gclsrc=aw.ds">
                <div class="produit_unique" id="produit1">
                    
                </div>
            </a>
            <a href="https://www.onatera.com/produit-huile-vegetale-helichryse-bio-50-ml-terraia,15780.html?LGWCODE=15780;105739;2445&utm_source=google&utm_medium=cpc&utm_campaign=search&gclid=CjwKCAjw_-D3BRBIEiwAjVMy7LTgFlVajQoO8oXTYwpZlVNvM9oeoNyKK5I1yQN9JONet4rgdVucohoCj-UQAvD_BwE&gclsrc=aw.ds">
                <div class="produit_unique" id="produit2">
                    
                </div>
            </a>
            <a href="https://www.onatera.com/produit-huile-essentielle-tea-tree-bio-100-ml-terraia,15710.html?LGWCODE=15710;105739;2445&utm_source=google&utm_medium=cpc&utm_campaign=search&gclid=Cj0KCQjwudb3BRC9ARIsAEa-vUtypBgxIPk_TLGgvMKoC5unSwK7i0eEoOwIi2tEcwn65GzICzlRoNkaAhKhEALw_wcB&gclsrc=aw.ds">
                <div class="produit_unique" id="produit3">
                
                </div>
            </a>
            <a href="https://www.onatera.com/produit-huile-essentielle-lavande-fine-bio-10-ml-terraia,11871.html">
                <div class="produit_unique" id="produit4">
        
                </div>
            </a>
            <a href="https://www.google.com/search?q=alcool+90+degre&tbm=isch&ved=2ahUKEwijxrjYpp_qAhVLrhoKHcxXBC8Q2-cCegQIABAA&oq=alcool+90+de&gs_lcp=CgNpbWcQARgBMgIIADICCAAyAggAMgIIADIECAAQGDIECAAQGDIECAAQGDIECAAQGDIECAAQGDIECAAQGDoGCAAQCBAeUO3dA1iN7gNg4ocEaABwAHgAgAFWiAH2ApIBATWYAQCgAQGqAQtnd3Mtd2l6LWltZw&sclient=img&ei=9tH1XqPzOMvcasyvkfgC&bih=750&biw=1536&client=firefox-b-d&hl=fr#imgrc=Pd4rxMHtPVDrpM">
                <div class="produit_unique" id="produit5">

                </div>
            </a>
        </div>
    </div>
</div>
@extends('layouts.footer')

@section('scripts')
@endsection
