@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection





<section class="jumbotron text-center">
    <div class="container">

        <h1 class="text-center" id="main_title">Notre boutique</h1>

        <p class="lead text-muted"> Bienvenue dans la boutique de CovidShop. Vous y trouverez tous nos articles vous
            permettant de vivre plus sereinement ! </p>
        <p>
{{--            <a href="#" class="btn btn-primary my-2">Catégorie 1</a>--}}
{{--            <a href="#" class="btn btn-primary my-2">Catégorie 2</a>--}}
{{--            <a href="#" class="btn btn-primary my-2">Catégorie 3</a>--}}
{{--            <a href="#" class="btn btn-primary my-2">Catégorie 4</a>--}}
        </p>
    </div>
</section>

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@foreach($articles->chunk(3) as $articleChunk)
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                @foreach($articleChunk as $article)
                    <div class="col-md-4">
                        <div class="card mb-4 shadow-sm">
                            <img class="bd-placeholder-img card-img-top" height="400rem"
                                 src="{{ $article->imageUrl }}" preserveAspectRatio="xMidYMid slice"
                                 focusable="false" role="img" aria-label="Placeholder: Thumbnail">
                            <rect width="100%" height="100%" fill="#55595c"/>
                            <div class="card-body">
                                <h4 class="card-title">{{$article->name}}</h4>
                                <p class="card-text">{{$article->description}}</p>
                                <div class="d-flex justify-content-between align-self-center mt-3">
                                    <div class="d-flex flex-row bd-highlight mb-3">
                                        <a href="{{ url('/articles/' . ($article->id)) }}" type="button"
                                           class="btn btn btn-outline-secondary mr-1"> Description
                                        </a>

                                        <form action="{{ route('cart.store') }}" method="POST">
                                            @csrf

                                            <input type="hidden" name="article_id" value="{{ $article->id }}">
                                            <button type="submit" class="btn btn btn-outline-secondary mr-1"><i
                                                    class="fas fa-cart-plus"></i></button>
                                        </form>

                                        <form method="POST" action="{{route('articles.destroy', $article->id)}}">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger"><i class="fas fa-trash mr-1"></i>
                                            </button>
                                        </form>
                                    </div>
                                    <div>
                                        <p class="font-weight-bold">{{$article->price}}€</p>
                                        <p class="text d-flex justify-content-end">Restants : {{$article->quantity}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endforeach
@extends('layouts.footer')

@section('scripts')
@endsection
