@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection

<div class="container_page">
    <h1 class="text-center" id="main_title">Ajouter un produit</h1>

    <div class="container_content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Ajouter un produit') }}</div>

                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ route('articles.store') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Name: </label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Catégorie: </label>
                                    <input type="number" class="form-control" id="category_id" name="category_id">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description: </label>
                                    <input type="text" class="form-control" id="description" name="description">
                                </div>
                                <div class="form-group">
                                    <label for="price">Prix: </label>
                                    <input type="number" class="form-control" id="price" name="price">
                                </div>
                                <div class="form-group">
                                    <label for="imageUrl">Image: </label>
                                    <input type="text" class="form-control" id="imageUrl" name="imageUrl">
                                </div>
                                <div class="form-group">
                                    <label for="quantity">Quantité: </label>
                                    <input type="number" class="form-control" id="quantity" name="quantity">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" value="Créer article">
                                    <input type="reset" class="btn btn-danger" value="Effacer">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@endsection
