@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection


<h1 class="text-center titleshow">Notre produit</h1>

<div class=" card mb-3 align-items-center container_cardProduct">
    <div class="card mb-4 shadow-sm" style="width: 25rem;">

        <img class="card-img-top" height="100%"
             src="{{ $article->imageUrl }}" preserveAspectRatio="xMidYMid slice"
             focusable="false" role="img" aria-label="Placeholder: Thumbnail">
        <rect width="100%" height="100%" fill="#55595c"/>
        <div class="card-body">
            <h4 class="card-title">{{$article->name}}</h4>
            <p class="card-text">{{$article->description}}</p>
            <div class="d-flex flex-row justify-content-between align-items-center mt-4">

                <form action="{{ route('cart.store') }}" method="POST">
                    @csrf

                    <input type="hidden" name="article_id" value="{{ $article->id }}">
                    <button type="submit" class="btn btn-dark">Ajouter au panier</button>
                </form>

                <p class="font-weight-bold">{{$article->price}}€</p>
                <p class="text">Restants : {{$article->quantity}}</p>
                <a href="/articles/{{$article->id}}/edit" class="btn btn-info"> Edit</a>
            </div>
        </div>
    </div>
</div>

<!-- START THE FEATURETTES -->

<div class="container_show">
    <h2 class="fournisseurshow">Pourquoi nous choisir ?</h2>

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">La Livraison ! <span class="text-muted">Rapide Simple Efficace</span></h2>
            <p class="lead"> Nous suivons vos colis et collaborons avec les transporteurs pour assurer la livraison
                de vos colis dans les meilleurs délais, tout en veillant à la sécurité de chacun.
                Nos produits vous seront livrés de 7 à 10 jours une fois l'expédition de la commande effectuée.</p>
        </div>
        <div class="col-md-5">
            <img class="bd-placeholder-img" width="350" height="250" src="../livraison.jpg">
        </div>
    </div>
    
    
    
    <div class="row featurette">
        <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">La disponibilité ! <span class="text-muted">Commander quand vous voulez</span>
            </h2>
            <p class="lead"> Nous faisons de notre mieux pour vous satisfaire et donc pour éviter qu'un de nos produits soit
                indisponible. Pour cela nous vérifions chaque jour
                si notre stock de marchandise est assez rempli. Vous pouvez commander sans vous inquiéter sur la
                disponibilité des produits que nous vendons sur notre site.</p>
        </div>
        <div class="col-md-5 order-md-1">
            <img class="bd-placeholder-img" width="600" height="250" src="../stockage2.jpg">
        </div>
    </div>
    
    
    
    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">La qualité ! <span class="text-muted">Faites nous confiance</span></h2>
            <p class="lead">Face à cette siutation exceptionnelle, notre enseigne s'assure de vous fournir des produits de
                qualité.
                Nos gels et masques respectent les normes de l'AFNOR; ils vous protégerons de la meilleure des façons contre
                le Covid-19.
            </p>
        </div>
    </div>
</div>



<!-- /END THE FEATURETTES -->

@extends('layouts.footer')

@section('scripts')
@endsection
