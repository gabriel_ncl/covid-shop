@extends('layouts.master')
@extends('layouts.navbar')

@section('title')

    Covid Shop

@endsection

@section('styles')
@endsection

<div class="container_page">
    <h1 class="text-center" id="main_title">Éditer un produit</h1>

    <div class="container_content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">{{ __('Éditer un produit') }}</div>

                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ route('articles.update', $article->id) }}" method="POST">
                                @csrf
                                {{ method_field('PUT') }}
                                <div class="form-group">
                                    <label for="name">Name: </label>
                                    <input type="text" class="form-control" id="name" name="name"
                                           value="{{$article->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Catégorie: </label>
                                    <input type="number" class="form-control" id="category_id" name="category_id"
                                           value="{{$article->category_id}}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description: </label>
                                    <input type="text" class="form-control" id="description" name="description"
                                           value="{{$article->description}}">
                                </div>
                                <div class="form-group">
                                    <label for="price">Prix: </label>
                                    <input type="number" class="form-control" id="price" name="price"
                                           value="{{$article->price}}">
                                </div>
                                <div class="form-group">
                                    <label for="imageUrl">Image: </label>
                                    <input type="text" class="form-control" id="imageUrl" name="imageUrl"
                                           value="{{$article->imageUrl}}">
                                </div>
                                <div class="form-group">
                                    <label for="quantity">Quantité: </label>
                                    <input type="number" class="form-control" id="quantity" name="quantity"
                                           value="{{$article->quantity}}">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-success" value="Editer l'article">
                                    <input type="reset" class="btn btn-danger" value="Effacer">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
@endsection
