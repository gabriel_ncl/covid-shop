<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = ['name', 'category_id', 'description', 'price', 'imageUrl', 'quantity'];

    public function order(){
        return $this->belongsTo('App\Order');
    }
    public function category(){
        return $this->hasOne('App\Category');
    }
    public function cart(){
        return $this->belongsTo('App\Cart');
    }
}
