<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function articles(){
        return $this->hasMany('App\Article');
    }
    public function users(){
        return $this->hasOne('App\User');
    }
}
