<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user(){
        return $this->hasOne('App\User');
    }

    public function articles(){
        return $this->hasMany('App\Article');
    }

}
